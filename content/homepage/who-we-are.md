---
title: "Who We Are"
header_menu_title: "Who We Are"
navigation_menu_title: "Who We Are"
weight: 2
header_menu: true
---

Wellness Physical Therapy is a mobile PT practice providing personalized care in the comfort of
your home. We are currently serving the areas of Conifer, Pine Junction, Morrison, and South
Evergreen. If you live outside of these areas and are interested in working with us, please reach
out – we may still be able to work with you, or help you find a great provider in your area.

---

## Our Approach

We provide Physical Therapy with a holistic approach, personalized for you! Each session may
include assessment, movement analysis and correction, exercise prescription, dry needling,
hands on therapy, pelvic floor assessment and treatment, balance, education, lifestyle coaching
and relaxation techniques. These services will be tailored to you to help you achieve your goals!
