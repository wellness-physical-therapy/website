---
title: "Contact"
weight: 99
header_menu: true
---

Want to learn more about Wellness Physical Therapy? Have additional questions?  Schedule a free 15 minute phone consult!

{{<icon class="fa fa-envelope">}}&nbsp;[jen@wellness-physicaltherapy.com](mailto:jen@wellness-physicaltherapy.com)

{{<icon class="fa fa-phone">}}&nbsp;[+1 970-363-4315](tel:+19703634315)

