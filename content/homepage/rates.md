---
title: "Rates"
weight: 4
header_menu: true
---
Due to the restrictions and time constraints that insurance places on care providers, Wellness
Physical Therapy does not accept insurance. This allows us to provide longer treatment sessions
focused on the things that matter to you!

Wellness Physical Therapy provides sessions for $150. We accept HSA/FSA, credit card, cash,
check, or Zelle.
