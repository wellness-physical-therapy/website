---
title: "Providers"
weight: 3
header_menu: true
---

<img src="/images/jennifer.jpg" width="200" />

Hi, I’m Jennifer! I have been a physical therapist since 2016. I love learning about my patients,
hearing their stories, and helping them to achieve their health goals! This process is highly
individualized, and I strive to provide the opportunity for patients to set the pace so that they
feel empowered.

While going through my own health challenges, I have learned a tremendous amount about
how various lifestyle factors and mindset can impact how we feel and function. This experience
has helped me relate to my patients and has made me a more compassionate health care
provider. I know what it’s like to be on the sidelines (or the couch), letting life pass by. By
working with me, you will have a provider that gets it.
Together we can make a plan for you to achieve your health and wellness goals.