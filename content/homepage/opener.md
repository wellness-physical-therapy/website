---
title: "Welcome"
weight: 1
header_menu: true
---

Wellness: the act of practicing healthy habits on a daily basis to attain better
physical and mental health outcomes, so that instead of just surviving, you&#39;re
thriving.

Working with Wellness Physical Therapy will help you to achieve your wellness
goals by addressing your physical health and the lifestyle factors that impact
health.

These lifestyle factors include: sleep, exercise, nutrition, stress management,
hydration, mindset, mindfulness and meditation. Numerous studies have shown
that addressing these factors can reduce pain by impacting the nervous system.

![targets](/images/mountain-logo.png)
